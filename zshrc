autoload -Uz compinit
compinit
bindkey -v


# SET OPTIONS ##############################################################
# If we have a glob this will expand it
setopt GLOB_COMPLETE
# beeps are annoying
setopt NO_BEEP
# Keep echo "station" > station from clobbering station
setopt NO_CLOBBER
# Case insensitive globbing
setopt NO_CASE_GLOB
# Be Reasonable!
setopt NUMERIC_GLOB_SORT
# I don't know why I never set this before.
setopt EXTENDED_GLOB
############################################################################


# HISTORY ##################################################################
HISTFILE=~/.zsh_histfile
HISTSIZE=1000
SAVEHIST=1000
# Save the time and how long a command ran
setopt EXTENDED_HISTORY
############################################################################


# COMPLETION ###############################################################
zstyle :compinstall filename '~/.zshrc'
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*' group-name ''
# case insensitive completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
# Have the newer files last so I see them first
zstyle ':completion:*' file-sort modification reverse
# color code completion!!!!  Wohoo!
zstyle ':completion:*' list-colors "=(#b) #([0-9]#)*=36=31"
# Separate man page sections.  Neat.
zstyle ':completion:*:manuals' separate-sections true
############################################################################


# KEY BINDS ################################################################
# Who doesn't want home and end to work?
bindkey '\e[1~' beginning-of-line
bindkey '\e[4~' end-of-line
# Meta-u to chdir to the parent directory
bindkey -s '\eu' '^Ucd ..; ls^M'
############################################################################


# EXPORT & ALIAS ###########################################################
export EDITOR=nano
alias ll='ls -alFG'
alias ls='ls -F'
alias grep='grep --color=always'
alias temp='sysctl dev.cpu.0.freq dev.cpu.0.temperature dev.cpu.2.temperature dev.cpu.4.temperature dev.cpu.6.temperature'
############################################################################


# PROMPT ###################################################################
PROMPT='%F{red}%?%f %T %F{blue}%n%f@%m [%B%1~%b]%S%#%s '
#RPROMPT='%(?..%{$fg_bold[red]%}[%? $(kill -l $?)]%{${reset_color}%} ) %F{red}%~'
RPROMPT=' %~'

