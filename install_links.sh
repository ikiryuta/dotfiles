#!/bin/sh
########################
# install.sh
# This script creates symlinks from ~/ to files in ~/dotfiles/
########################


# Variables
dir=~sleepy/dotfiles
olddir=~/dotfiles_old
# List of files to symlink
files="vimrc zshrc nanorc"



# Create dotfiles_old in homedir
mkdir -p $olddir
cd $dir

for file in $files; do
# if file does not exist
    if [ ! -f ~/.$file ]; then
        echo "Creating symlink $file to $dir/$file"
        ln -s $dir/$file ~/.$file
# if file exists and is symlink
    elif [ -h ~/.$file ]; then
        echo "$file symlink already exists"
# if file exists and not symlink
    elif [ -f ~/.$file ]; then
        echo "Backing up existing $file to $olddir/$file"
        mv ~/.$file $olddir/$file
        echo "Creating symlink $file to $dir/$file"
        ln -s $dir/$file ~/.$file
    fi
done

